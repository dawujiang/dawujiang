//弹出两次后,不再弹出信息框
var count = 1; //定义一个计数器
var id = setInterval("alertMessage()", 2000);

function alertMessage() {
	alert("我弹出来了");
	count++;
	if(count > 2) {
		//取消周期性执行,不要在调用alertMessage()函数
		clearInterval(id);
	}
}